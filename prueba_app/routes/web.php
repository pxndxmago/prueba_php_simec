<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', function () {
    return view('home');
})->name('home');

// Ruta para el juego Mastermind
Route::get('/mastermind', function () {
    return view('mastermind');
})->name('mastermind');

// Ruta para el juego Mastermind
Route::get('/newScore/{name}/{score}', 'ScoreController@store')->name('newScore');
Route::get('/scores', 'ScoreController@list')->name('scores');
