@extends('layouts.base')
@section('main_content')
    <!-- Content Wrapper. Contains page content -->
   
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Juegos</h1>
                        <a href="{{ route('scores') }}" class="btn btn-primary"> VER LAS PUNTUACIONES</a>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    {{-- <div class="col-lg-3 col-6">
                        <!-- small box -->

                        <div class="small-box bg-info">
                            
                            <div class="icon">
                                <img style="width: 100%; height: auto;" src="img/ROCK-PAPER-SCISSORS-LIZZARD-SPOCK.jpg"
                                    alt="">
                            </div>
                            <a href="{{route('mastermind')}}" class="small-box-footer">Jugar <i class="fas fa-gamepad"></i></a>
                        </div>
                    </div> --}}
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->

                        <div class="small-box bg-success">

                            <div class="icon">
                                <img style="width: 100%; height: auto;" src="img/mastermind.jpg" alt="">
                            </div>
                            <a href="{{ route('mastermind') }}" class="small-box-footer">Jugar MASTERMIND <i
                                    class="fas fa-gamepad"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->

                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
    </div>
    <!-- Main content -->

@endsection
