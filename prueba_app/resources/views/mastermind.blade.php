@extends('layouts.base')
@section('main_content')
    <!-- Content Wrapper. Contains page content -->
    <div class="modal fade" id="winModal" tabindex="-1" role="dialog" aria-labelledby="winModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="winModalLabel">Juego finalizado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ingresa tu nombre para guardar tu puntuacion</label>
                        <input id="name" type="text" class="form-control" placeholder="Tu nombre">
                        <label for="">Tu puntuacion fue: <div id="score"></div></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnEnviar" type="button" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal de instrucciones --}}
    <div class="modal fade" id="instructionsModal" tabindex="-1" role="dialog" aria-labelledby="instructionsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="instructionsModalLabel">Instrucciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Si hemos colocado una ficha del color correcto en la posición adecuada, nos aparecerá un punto
                        negro. <br>
                        Si hemos acertado el color de la ficha pero la posición no es correcta, aparecerá un punto
                        blanco. 
                        Y si por el contrario no existe ninguna ficha de ese color en el juego el punto no cambiará
                        de color. <br>
                        Los colores de los puntos nos indican la cantidad de piezas bien o mal colocadas pero no
                        nos indican a que ficha se refieren. <br>
                        Es simplemente un contador global de las fichas bien colocadas, las fichas mal situadas y las fichas que no aparecen en la combinación.<br>

                        Sigue las pistas de cada jugada para planear la combinación de colores del siguiente turno. <br>
                        Si acertamos la combinación, los cuatro puntos aparecerán de color negro y se mostrará la combinación
                        oculta en la parte inferior.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">MASTERMIND</h1>
                        <a href="{{ route('scores') }}" class="btn btn-primary"> VER LAS PUNTUACIONES</a>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <style>
            .piece-container {
                background-color: rgb(243, 214, 203);
                margin: 10px;
            }

        </style>
        <section class="content">
            <div class="container-fluid">
                <div class="row ">
                    <div style="background-color: beige; height: 500px; position: sticky; top: 0;" class="col-md-3 my-3">
                        <div class="row">
                            <div class="col-4">
                                <div class="draggable color-piece" id="red"
                                    style="width: 80px; height: 80px; background-color: red">
                                </div>

                            </div>
                            <div class="col-4">
                                <div class="draggable color-piece" id="blue"
                                    style="width: 80px; height: 80px; background-color: blue">
                                </div>

                            </div>
                            <div class="col-4">
                                <div class="draggable color-piece" id="green"
                                    style="width: 80px; height: 80px; background-color: lightgreen">
                                </div>

                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-4">
                                <div class="draggable color-piece" id="purple"
                                    style="width: 80px; height: 80px; background-color: purple"></div>

                            </div>
                            <div class="col-4">
                                <div class="draggable color-piece" id="orange"
                                    style="width: 80px; height: 80px; background-color: orange"></div>

                            </div>
                            <div class="col-4">
                                <div class="draggable color-piece" id="yellow"
                                    style="width: 80px; height: 80px; background-color: yellow"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-1" id="div2">

                            </div>
                            <div class="col-md-1 piece-container droppable line-1" id="div3">

                            </div>
                            <div class="col-md-1 piece-container droppable line-1" id="div4">

                            </div>
                            <div class="col-md-1 piece-container droppable line-1" id="div5">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-1-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="1" class="btn btn-primary verify-btn">Verificar</button>
                            </div>

                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-2" id="div2">

                            </div>
                            <div class="col-md-1 piece-container droppable line-2" id="div3">

                            </div>
                            <div class="col-md-1 piece-container droppable line-2" id="div4">

                            </div>
                            <div class="col-md-1 piece-container droppable line-2" id="div5">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-2-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="2" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line3">

                            </div>
                            <div class="col-md-1 piece-container droppable line-3">

                            </div>
                            <div class="col-md-1 piece-container droppable line-3">

                            </div>
                            <div class="col-md-1 piece-container droppable line-3">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-3-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="3" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-4">

                            </div>
                            <div class="col-md-1 piece-container droppable line-4">

                            </div>
                            <div class="col-md-1 piece-container droppable line-4">

                            </div>
                            <div class="col-md-1 piece-container droppable line-4">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-4-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="4" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-5">

                            </div>
                            <div class="col-md-1 piece-container droppable line-5">

                            </div>
                            <div class="col-md-1 piece-container droppable line-5">

                            </div>
                            <div class="col-md-1 piece-container droppable line-5">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-5-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="5" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-6">

                            </div>
                            <div class="col-md-1 piece-container droppable line-6">

                            </div>
                            <div class="col-md-1 piece-container droppable line-6">

                            </div>
                            <div class="col-md-1 piece-container droppable line-6">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-6-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="6" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-7">

                            </div>
                            <div class="col-md-1 piece-container droppable line-7">

                            </div>
                            <div class="col-md-1 piece-container droppable line-7">

                            </div>
                            <div class="col-md-1 piece-container droppable line-7">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-7-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="7" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div style="height: 100px;" class="col-md-3" id="div1">

                            </div>
                            <div class="col-md-1 piece-container droppable line-8">

                            </div>
                            <div class="col-md-1 piece-container droppable line-8">

                            </div>
                            <div class="col-md-1 piece-container droppable line-8">

                            </div>
                            <div class="col-md-1 piece-container droppable line-8">

                            </div>
                            <div class="col-md-1">
                                <div class="row" id="line-8-results">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <button id="8" disabled=true class="btn btn-primary verify-btn">Verificar</button>
                            </div>
                        </div>
                        <hr>
                        {{-- QUITAR display: none; para visualizar el resultado desde el inicio --}}
                        <div id="result" class="row" style="display: none;">
                            <div style="height: 100px;" class="col-md-3">

                            </div>
                            <div class="col-md-1 piece-container code " id="cod-1">

                            </div>
                            <div class="col-md-1 piece-container code" id="cod-2">

                            </div>
                            <div class="col-md-1 piece-container code" id="cod-3">

                            </div>
                            <div class="col-md-1 piece-container code" id="cod-4">

                            </div>
                            <div class="col-md-1" id="div5">
                                <div class="row" id="line-3-results">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.container-fluid -->
        </section>
    </div>
    <!-- Main content -->

@endsection

@section('jss')
    <script src="{{ asset('js/drop.js') }}"></script>
@endsection
