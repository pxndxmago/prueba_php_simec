<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Score;

class ScoreController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function store($name, $score)
    {
        // $name = $request->name;
        // $score = $request->score;

        $new_score = new Score();
        $new_score->name = $name;
        $new_score->score = $score;

        $new_score->save();

        
    }

    public function list()
    {
        // $name = $request->name;
        // $score = $request->score;

        $scores =Score::all();
        return view('scores', ['scores' => $scores]);

        
    }
}
