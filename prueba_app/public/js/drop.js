var code = [];
var score = 96; //Corresponde al mayor numero de puntos que se pueden obtener, de acuerdo a los intentos y fallos se ira restando puntos
//Genera un numero aleatorio entre min y max 
$('#instructionsModal').modal('show');

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

// Se genera el codigo aleatorio
function generateCode() {
    var colors = [

        '<div class="draggable color-piece" id="red"style="width: 80px; height: 80px; background-color: red"></div>',
        '<div class="draggable color-piece" id="blue"style="width: 80px; height: 80px; background-color: blue"></div>',
        '<div class="draggable color-piece" id="green"style="width: 80px; height: 80px; background-color: lightgreen"></div>',
        '<div class="draggable color-piece" id="yellow"style="width: 80px; height: 80px; background-color: yellow"></div>'

    ];

    $('#cod-1').append(colors[getRandomInt(0, 3)]);
    $('#cod-2').append(colors[getRandomInt(0, 3)]);
    $('#cod-3').append(colors[getRandomInt(0, 3)]);
    $('#cod-4').append(colors[getRandomInt(0, 3)]);

    $('.code')
        .each(function () {
            code.push($(this).find('.color-piece').first().attr("id"));
        });

}


generateCode();




$('.verify-btn').on('click', function () {
    var line = $(this).attr('id');
    // Desbloquea progresivamente los botones de verificacion
    if (line < 8) {
        $('#' + (parseInt(line) + 1)).prop("disabled", false);
    }

    var resultContainer = 'line-' + line + '-results';
    console.log("line: " + line);
    console.log("resultContainer: " + resultContainer);

    var currentLine = 'line-' + line;
    var colors = [];
    var codeCopy = [...code];
    var blackCount = [];
    var whiteCount = [];
    $('#' + resultContainer).empty();
    $('.' + currentLine)
        .find('.color-piece')
        .each(function () {
            colors.push($(this).attr("id"));
        });

    // Verificamos los aciertos de color y posicion
    colors.forEach((color, index) => {
        if (codeCopy[index] == color)
            blackCount++;

    });

    // Verificamos los aciertos de color
    colors.forEach((color, index) => {
        if (codeCopy.includes(color)) {
            whiteCount++;
            codeCopy.splice(codeCopy.indexOf(color), 1);
        }
    });

    whiteCount = whiteCount - blackCount;

    if (blackCount == 4) {
        $('#score').append(score);
        $('#winModal').modal('show');

        $('#result').show();
        alert("HAS GANADOOOO");
        return;
    };
    if (line == 8)
        $('#result').show();

    for (let index = 0; index < blackCount; index++) {
        $('#' + resultContainer).append('<div style="height: 40px; width: 40px; background-color: #0000005e; padding:10px;" class="col-md-6"><div style="height: 20px; width: 20px; background-color: black;"></div></div>');
    }

    for (let index = 0; index < whiteCount; index++) {
        $('#' + resultContainer).append('<div style="height: 40px; width: 40px; background-color: #0000005e; padding:10px;" class="col-md-6"><div style="height: 20px; width: 20px; background-color: white;"></div></div>');
    }

    //Se evaluan los puntos
    if (blackCount < 4) {
        score = score - (blackCount * 1);
        score = score - (whiteCount * 2);
        score = score - ((4 - whiteCount - blackCount) * 3);
    }

    $(this).prop("disabled", true);
});

// Configuracion de las fichas
$('.draggable').draggable({
    revert: "invalid",
    stack: ".draggable",
    helper: 'clone'
});

// Configuracion de los contenedores
$('.droppable').droppable({
    accept: ".draggable",
    accept: ".draggable",

    drop: function (event, ui) {
        var droppable = $(this);
        var draggable = ui.draggable;
        // Move draggable into droppable
        var drag = $('.droppable').has(ui.draggable).length ? draggable : draggable.clone().draggable({
            revert: "invalid",
            stack: ".draggable",
            helper: 'clone'
        });
        drag.appendTo(droppable);
        draggable.css({
            float: 'left'
        });
    },
    // drop: function (event, ui) {

    //     var droppable = $(this);
    //     var draggable = ui.draggable;

    //     // Move draggable into droppable
    //     draggable.appendTo(droppable);
    //     draggable.css({
    //         top: '1px', left: '1px'
    //     });
    // }
});

// Envia el nombre junto con la puntuacion
$('#btnEnviar').on('click', function () {
    var name = $('#name').val();
    $.ajax({
        url: "http://localhost/newScore/" + name + "/" + score,
        type: 'GET',
        complete: function () {
            $('#winModal').modal('hide');


        }
    });
});